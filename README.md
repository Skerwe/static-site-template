# Static Website Starter Template

[![Uses HTML](https://forthebadge.com/images/badges/uses-html.svg)](https://developer.mozilla.org/en-US/docs/Web/HTML)
[![Uses CSS](https://forthebadge.com/images/badges/uses-css.svg)](https://developer.mozilla.org/en-US/docs/Web/CSS)
[![Uses JavaScript](https://forthebadge.com/images/badges/uses-js.svg)](https://developer.mozilla.org/en-US/docs/Web/JavaScript)

Base static web development starter template. This is the starting point for a complete static website.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

1. Make sure [Node.js](https://nodejs.org/en/) is installed.

### Quick Start

Clone the repository with the name of your new project:  
`git clone https://Skerwe@bitbucket.org/indiesagtewerke/static-site-template.git <project-name>`

1. In bash/terminal/command line, `cd` into your project directory.
2. Run `npm install` to install required files and dependencies.
3. When it's done installing, run one of the tasks to get going:
    - `npm run dev` automatically compiles files and applies changes using the [Parcel bundler](https://parceljs.org/) when you make changes to your source files.
    - `npm run build` to run in production mode.

## Testing

I recommend using [TestCafe](https://devexpress.github.io/testcafe/) to test static sites.

## License

The source code is free -- see the [UNLICENSE](UNLICENSE) file for details
